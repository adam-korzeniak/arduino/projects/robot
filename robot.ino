#define L_PWM 5
#define L_DIR 4
#define R_PWM 6
#define R_DIR 9

#define FORWARD 0
#define BACKWARD 1
  
#define PWM_MAX 165
 
void setup() {
  pinMode(L_DIR, OUTPUT);
  pinMode(R_DIR, OUTPUT);
  pinMode(L_PWM, OUTPUT);
  pinMode(R_PWM, OUTPUT);
}
 
void loop() {
  leftMotor(95);
  rightMotor(95);
  delay(2000);
//  leftMotor(40);
//  rightMotor(40);
  delay(100);
//  leftMotor(20);
//  rightMotor(20);
  delay(100);
  stopMotors();

  delay(2000);
  
  leftMotor(-50);
  rightMotor(-50);
  delay(500);
  leftMotor(-40);
  rightMotor(-40);
  delay(500);
  leftMotor(-30);
  rightMotor(-30);
  delay(500);
  leftMotor(-20);
  rightMotor(-20);
  delay(100);
  stopMotors();
  
  delay(10000);
}

void leftMotor(int speed) {
  if (speed >= 0) {
    digitalWrite(L_DIR, FORWARD);
  } else {
    digitalWrite(L_DIR, BACKWARD);
  }
  
  if (abs(speed) > 100) {
    return;
  }
  speed = map(abs(speed), 0, 100, 0, PWM_MAX);
  analogWrite(L_PWM, speed);
}

void rightMotor(int speed) {
  if (speed >= 0) {
    digitalWrite(R_DIR, FORWARD);
  } else {
    digitalWrite(R_DIR, BACKWARD);
  }
  
  if (abs(speed) > 100) {
    return;
  }
  speed = map(abs(speed), 0, 100, 0, PWM_MAX);
  analogWrite(R_PWM, speed);
}
  
void stopMotors() {
  analogWrite(L_PWM, 0);
  analogWrite(R_PWM, 0);
}
